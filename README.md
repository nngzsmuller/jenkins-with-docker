The Docker image built from this repo contains the official Jenkins LTS release with an added docker-ce package so Jenkins can launch Docker containers while itself is running in a container.

See the "description" label of the image for details on how to use this image.
